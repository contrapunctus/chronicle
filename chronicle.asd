(defsystem     "chronicle"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      ("contrapunctus <contrapunctus at disroot dot org>"
                "John Lorentzson (Duuqnd)")
  :maintainer  ("contrapunctus <contrapunctus at disroot dot org>"
                "John Lorentzson (Duuqnd)")
  :description "CLIM calendar application"
  :depends-on  (#:mcclim #:local-time #:anathema #:chronicity
                         #:closer-mop #:str #:messagebox)
  :serial      t
  :components  ((:module "src/core/"
                 :components ((:file "package")
                              (:file "backend")
                              (:file "calendar")
                              (:file "entries")
                              (:file "reminders")
                              (:file "time")))))
