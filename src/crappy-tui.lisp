(in-package :cl)
(defpackage #:chronicle.crappy-tui
  (:use #:cl)
  (:local-nicknames (#:c #:chronicle)
                    (#:t #:local-time))
  (:export #:calendar-top-level))
(in-package #:chronicle.crappy-tui)

(defvar *active-calendar*)

(defparameter *command-table*
  '(#\a display-all-events
    #\d describe-event
    #\f display-future-events
    #\c select-calendar
    #\q quit-calendar
    #\n new-event
    #\w write-calendar))

(defun command (cmd-string)
  (handler-case
      (let ((command-char (elt cmd-string 0)))
        (funcall (getf *command-table* command-char) (subseq cmd-string 1)))
    (no-such-event (c) (princ c))))

(define-condition quit ()
  ())

(define-condition no-such-event ()
  ((%event-number :accessor event-number :initarg :number))
  (:report
   (lambda (condition stream)
     (format stream "No such event ~D.~%" (event-number condition)))))

(defun calendar-top-level ()
  (handler-case
      (loop
        (write-char #\:)
        (force-output)
        (with-simple-restart (restart "Return to calendar top level.")
          (command (read-line))))
    (quit () (format t "Exiting calendar top level."))))

(defun select-calendar (args)
  (handler-case
      (progn
        (setf *active-calendar* (c:load-calendar-from-file args))
        (format t "Loaded calendar.~%"))
    (error (c) (format t "Failed to load calendar. Error:~%~A~%" c))))

(defun write-calendar (args)
  (declare (ignore args))
  (c:save-calendar-to-file *active-calendar*))

(defun print-event (index event)
  (format t "~D. \"~A\" ~A ~~ ~A~%"
          index (c:name event)
          (c:timestamp-pretty-string (c:start-time event))
          (if (c:end-time event)
              (c:timestamp-pretty-string (c:end-time event))
              nil)))

(defun display-all-events (args)
  (let ((start (if (string= args "")
                   nil
                   (quick-and-dirty-timestamp args))))
    (loop :for event :in (c:sorted-events *active-calendar*)
          :for index :from 1
          :when (or (null start) (t:timestamp>= (c:start-time event) start))
            :do (print-event index event))))

(defun display-future-events (args)
  (declare (ignore args))
  (let ((future (c:future-events *active-calendar*)))
    (loop :for event :in (c:sorted-events *active-calendar*)
          :for index :from 1
          :when (member event future) :do
            (print-event index event))))

(defun describe-event (args)
  (when (or (> (parse-integer args) (length (c:list-events *active-calendar*)))
            (minusp (parse-integer args)))
    (error 'no-such-event :number (parse-integer args)))
  (let ((event (nth (1- (parse-integer args)) (c:sorted-events *active-calendar*))))
    (format t "~A~%Location: ~A~%Starts ~A~%" (c:name event)
            (c:location event)
            (c:timestamp-pretty-string (c:start-time event)))
    (when (c:end-time event)
      (format t "Ends ~A~%" (c:timestamp-pretty-string (c:end-time event))))
    (when (c:lasts-all-day-p event)
      (format t "Lasts all day.~%"))
    (format t "~%~A~%" (c:notes event))))

(defun make-timestamp-tui (year month day &optional (hour 0) (minute 0) (sec 0))
  (c:make-timestamp year month day :hour hour :minute minute :second sec))

(defun quick-and-dirty-timestamp (string)
  (if (string= "" string)
      nil
      (apply 'make-timestamp-tui (mapcar #'parse-integer (str:split #\Space string)))))

(defun new-event (args)
  (declare (ignore args))
  (unless (boundp '*active-calendar*)
    (error "Can't create new event without active calendar."))
  (flet ((prompt (message &optional (term-char #\Newline))
           (format t "~A: " message)
           (force-output)
           ;; TODO: use term-char
           (read-line)))
    (let ((event (make-instance 'c:event
                                :name (prompt "Name")
                                :notes (prompt "Notes (end with \\)")
                                :start-time (quick-and-dirty-timestamp (prompt "Start time"))
                                :end-time (quick-and-dirty-timestamp (prompt "End time"))
                                :lasts-all-day-p (prompt "Lasts all day? (T or NIL)")
                                :location (prompt "Location"))))
      (c:add-event *active-calendar* event))))

(defun quit-calendar (args)
  (declare (ignore args))
  (signal 'quit))
