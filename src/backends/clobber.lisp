(in-package :cl)
(defpackage :chronicle.clobber
  (:use :cl)
  (:local-nicknames (:cal :chronicle)
                    (:db :clobber)
                    (:t :local-time)))
(in-package :chronicle.clobber)

(cal:define-backend backend (cal:backend cal:file-backend-mixin) ()
  (:default-initargs :extension "clob"))

(db:define-save-info cal:calendar
  (:name name)
  (:events events))

(defparameter *calendars* nil)

(db:define-save-info cal:entry
  (:name name)
  (:category category)
  (:note note)
  (:location location)
  (:start start)
  (:end end)
  (:repeat repeat))

(defun apply-transaction (transaction)
  (apply (symbol-function (first transaction))
         (rest transaction)))

(defun execute (transaction-log function &rest arguments)
  (apply function arguments)
  (db:log-transaction (cons function arguments)
                      transaction-log))

;; Calendar operations
(defun add-calendar (calendar)
  (push calendar *calendars*))

(defmethod cal:add-calendar ((backend backend) (calendar cal:calendar))
  (db:with-transaction-log (log (file backend) #'apply-transaction)
    (execute log 'add-calendar calendar)))

(defun remove-calendar (calendar)
  (setf *calendars*
        (remove calendar *calendars* :test #'cal:calendar-equal)))

(defmethod cal:remove-calendar ((backend backend) (calendar cal:calendar))
  (db:with-transaction-log (log (file backend) #'apply-transaction)
    (execute log 'remove-calendar calendar)))

;; Entry operations
(defun update-calendar (old new)
  (setf *calendars* (subst new old *calendars* :test #'calendar-equal)))

(defun add-entry (entry calendar)
  (update-calendar calendar (progn
                              (push entry (entries calendar))
                              calendar)))

(defmethod cal:add-entry
    ((backend backend) (entry cal:entry) (calendar cal:calendar))
  (db:with-transaction-log (log (file backend) #'apply-transaction)
    (execute log 'add-entry entry calendar)))

(defmethod cal:remove-entry
    ((backend backend) (entry cal:entry) (calendar cal:calendar))
  (db:with-transaction-log (log (file backend) #'apply-transaction)
    (execute log 'remove-entry entry calendar)))
