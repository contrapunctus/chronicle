(in-package :cl)
(defpackage :chronicle.sexp
  (:use :cl)
  (:local-nicknames (:c :chronicle))
  (:export #:save-calendar-to-file #:load-calendar-from-file))
(in-package :chronicle.sexp)

(c:define-backend backend (c:backend c:file-backend-mixin) ()
  (:default-initargs :extension "sexp"))

(defvar *calendar-save-path* #p".")

(defun get-saveable-slot-values (obj)
  (let* ((class (class-of obj))
         (slots (remove-if-not 'c2mop:slot-definition-initargs (c2mop:class-slots class))))
    (loop :for slot :in slots
          :collect (list (first (c2mop:slot-definition-initargs slot))
                         (simplify-for-saving (slot-value obj (c2mop:slot-definition-name slot)))))))

(defun make-object-saveable (obj)
  (append (list (class-name (class-of obj)))
          (get-saveable-slot-values obj)))

(defgeneric simplify-for-saving (obj)
  (:documentation "Returns an object that can be serialized."))

(defmethod simplify-for-saving ((obj string))
  obj)

(defmethod simplify-for-saving ((obj number))
  obj)

(defmethod simplify-for-saving ((obj symbol))
  obj)

(defmethod simplify-for-saving ((obj list))
  (mapcar 'simplify-for-saving obj))

(defmethod simplify-for-saving ((obj entry))
  (make-object-saveable obj))

(defmethod simplify-for-saving ((obj t:timestamp))
  (list :timestamp (t:format-timestring nil obj)))

(defun save-as-sexps (save-obj stream)
  (dolist (event save-obj)
    (write-char #\( stream)
    (dolist (obj event)
      (write obj :stream stream :pretty t)
      (terpri stream))
    (write-char #\) stream)))

(defun save-calendar-to-file (calendar)
  (let ((*package* (find-package '#:chronicle.sexp)))
    (with-open-file (stream (merge-pathnames
                             (format nil "~A.calendar" (name calendar))
                             *calendar-save-path*)
                            :direction :output
                            :if-exists :supersede
                            :if-does-not-exist :create)
      (format stream ";;;; -*- mode: lisp-data -*-~%")
      (format stream "\"~A\"~%" (name calendar))
      (save-as-sexps (simplify-for-saving
                      (coerce (sorted-events calendar) 'list))
                     stream)
      T)))

(defun parse-saved-obj (obj-list)
  (cond ((and (listp obj-list) (eq :timestamp (first obj-list)))
         (t:parse-timestring (second obj-list)))
        ((and (listp obj-list) (keywordp (first obj-list))
              (= (length obj-list) 2)
              (list (first obj-list) (parse-saved-obj (second obj-list)))))
        ((and (listp obj-list) (ignore-errors (find-class (first obj-list))))
         (apply 'make-instance
                (first obj-list)
                (apply 'append (mapcar 'parse-saved-obj (rest obj-list)))))
        ((listp obj-list)
         (mapcar 'parse-saved-obj obj-list))
        (t obj-list)))

(defun load-calendar-from-file (calendar-name)
  (with-open-file (stream (merge-pathnames
                           (format nil "~A.calendar" calendar-name)
                           *calendar-save-path*))
    (when (string-not-equal (read stream) calendar-name)
      (error "Calendar name and file name don't match."))
    (let ((events '())
          (*package* (find-package '#:chronicle.sexp)))
      (handler-case
          (loop
            (let ((obj (parse-saved-obj (read stream))))
              (cond ((typep obj 'entry)
                     (push obj events))
                    ((and (typep obj 'reminder) (not (has-fired-p obj)))
                     (register-reminder obj))
                    (t (error "Invalid object in calendar.")))))
        (end-of-file ()
          (make-instance 'c:calendar
                         :events events :name calendar-name))))))

(defmethod c:add-entry ((backend backend) (calendar calendar) (event entry))
  (push event (c:events calendar))
  event)

(defmethod c:remove-entry ((backend backend) (calendar calendar) (event entry))
  (setf (c:events calendar) (remove event (c:events calendar))))

(defmethod c:list-events ((calendar calendar))
  (copy-list (c:events calendar)))

(defmethod c:sorted-events ((calendar calendar))
  (sort (c:list-events calendar)
        (lambda (a b)
          (t:timestamp< (c:start-time a)
                        (c:start-time b)))))

(defmethod c:future-events ((calendar calendar))
  (remove-if-not (lambda (x) (or (c:in-the-future-p x)
                                 (c:happening-now-p x)))
                 (c:list-events calendar)))
