(in-package :chronicle.clim)

(defclass input-pane (common-pane) ()
  (:documentation "Pane to display a form for entering calendar events."))

(defmethod display-pane (frame (stream input-pane)))
