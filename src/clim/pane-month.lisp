(in-package :chronicle.clim)

(defclass month-pane (common-pane)
  (;; (%date :initarg :date :accessor date)
   (%month :initarg :month :accessor month :initform (t:today))
   (%dates :initarg :dates :accessor dates
           :documentation
           "Instances of `local-time:timestamp' representing days in `month'.")
   (%view :initarg :view)
   (%calendar :initarg :calendar
              :initform (make-instance 'cal:calendar)
              :accessor calendar
              :documentation "The instance of `chronicle:calendar' in use."))
  (:documentation "Application pane displaying a single month."))

(defun make-empty-vector ()
  (make-array 1 :fill-pointer 0
                :adjustable t
                :initial-element nil))

(defun month-dates (month-ts)
  (t:with-decoded-timestamp (:month month :year year) month-ts
    (loop :with dates = (make-empty-vector)
          :for date :from 1 :to (t:days-in-month month year)
          :do (vector-push-extend
               (t:adjust-timestamp month-ts
                 (set :day-of-month date))
               dates)
          :finally (setf (cursor (aref dates 0)) t)
                   (return dates))))

(defmethod initialize-instance :after
     ((pane month-pane) &rest initargs &key &allow-other-keys)
  (unless (slot-boundp pane '%dates)
    (setf (dates pane) (month-dates (month pane)))))

(defmethod (setf month) (new-value (pane month-pane))
  (setf (slot-value pane '%month)  new-value
        (dates pane)  (month-dates (month pane))))

(defun display-month-year (stream timestamp)
  (formatting-row (stream)
    (formatting-cell (stream :align-x :center)
      (at:with-style (stream *header-month*)
        (t:format-timestring stream timestamp :format '(:long-month #\Space :year)))
      (format stream "~%"))))

(defun display-weekday-names (stream)
  (formatting-row (stream)
    (loop :for dow-i :from 0 :to 6
          :for dow :in '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
          :when (or (= dow-i 0) (= dow-i 6))
            :do (at:with-style (stream *header-weekend*)
                  (formatting-cell (stream :align-x :center)
                    (format stream "~A " dow)))
          :else :do
            (at:with-style (stream *header-weekday*)
              (formatting-cell (stream :align-x :center)
                (format stream "~A " dow))))))

(define-presentation-method present :around
  (file (type cursor-mixin) (pane month-pane) (view view) &key)
  (cond ((cursor file)
         (surrounding-output-with-border (pane :move-cursor nil :padding 2)
           (call-next-method)))
        (t (call-next-method))))

(define-presentation-method present
    (object (type calendar-date-mixin) (stream month-pane) view
            &key acceptably for-context-type)
  (let ((date (t:timestamp-day object)))
    ;; highlight today's date
    (cond ((t:timestamp= object (t:today))
           (at:with-style (stream *today*)
             (format stream "~D" date)))
          (t (format stream "~D" date)))))

(defun display-calendar (pane month-ts)
  (t:with-decoded-timestamp (:month month :year year) month-ts
    (formatting-table (pane)
      (loop
        :with dates = (dates pane)
        ;; day of week on start of month
        :with start-day = (t:timestamp-day-of-week
                           (t:adjust-timestamp month-ts (set :day-of-month 1)))
        :with days-in-month = (t:days-in-month month year)
        :with first-week = t
        :with date = 1
        ;; Print weekday header
          :initially (display-weekday-names pane)
        ;; Print dates
        :while (not (> date days-in-month))
        :do (formatting-row (pane)
              (loop
                :for dow :from 0 :to 6 :do
                  (formatting-cell (pane :align-x :center
                                         :align-y :center)
                    (when (and (<= date days-in-month)
                               (or (not first-week)
                                   (>= dow start-day)))
                      (present (aref dates (1- date)) 'calendar-date-mixin :stream pane)
                      (incf date)))))
            (setf first-week nil)))))

;; We draw two nested tables, for sake of centering the month and year
;; name relative to the calendar
(defmethod display-pane (frame (pane month-pane))
  (let ((month (month pane)))
    (formatting-table (pane)
      (display-month-year pane month)
      (formatting-row (pane)
        (formatting-cell (pane)
          (display-calendar pane month))))))
