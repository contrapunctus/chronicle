(in-package :chronicle.clim)

(defmacro make-pane-helper (type)
  `(multiple-value-bind (pane stream)
       (make-clim-stream-pane :type (quote ,type))
     (setf (,type *application-frame*) stream)
     pane))

(define-application-frame frame ()
  ((%month-pane :initarg :month-pane :accessor month-pane)
   (%input-pane :initarg :input-pane :accessor input-pane))
  (:panes (calendar (make-pane-helper month-pane))
          (input (make-pane-helper input-pane))
          (interactor :interactor))
  (:layouts (default calendar)
            (input   (vertically () calendar input)))
  (:menu-bar t)
  (:pretty-name "Calendar")
  (:reinitialize-frames t))

(defun run ()
  (let ((frame (find-application-frame 'frame)))
    (when i:*policy*
      (i:apply-policy i:*policy* 'frame))
    frame))

;; We want to show a pane for input, but only when input is desired.
;; Having the input fields hide the application pane is less than
;; ideal, as it would impede the use of presentations. Having a
;; separate input pane be visible at all times would not be desirable,
;; as it uses screen space unnecessarily.

;; Changing the layout "breaks" out of the command loop iteration, so
;; we change the layout and schedule com-add-entry-helper to be
;; executed after the layout change is complete.

;; If we were to attempt to do this entirely in `com-add-entry', the
;; command would need to be issued twice before the form is displayed.

(define-frame-command (com-add-entry :menu t :name t) ()
  (unwind-protect
       (setf (frame-current-layout *application-frame*) 'input)
    (change-space-requirements (input-pane *application-frame*)
                               :resize-frame t)
    (execute-frame-command *application-frame*
                           '(com-add-entry-helper))))

(define-frame-command (com-add-entry-helper) ()
  (let* ((stream    (input-pane *application-frame*))
         (*query-io*  stream)
         (*standard-output* stream)
         name category location note start end repeat calendar)
    (macrolet ((prompt (var type prompt &rest keys)
                 `(progn
                    (terpri)
                    (setf ,var (accept (quote ,type)
                                       :prompt ,prompt :stream stream ,@keys)))))
      (window-clear stream)
      (unwind-protect
           (progn
             (accepting-values (stream :resynchronize-every-pass t)
               (prompt calendar string "Calendar")
               (prompt name string "Name")
               (prompt category string "Category")
               (prompt location string "Location")
               (prompt note string "Note")
               (prompt start string "Start")
               (prompt end string "End")
               (prompt repeat string "Repeat")
               ;; (terpri)
               ;; (setf all-day-p (accept 'boolean :stream stream
               ;;                                  :prompt "Lasts all day?"
               ;;                                  :view +toggle-button-view+
               ;;                                  :default all-day-p))
               )
             (cal:add-entry
              (make-instance 'cal:event
                             :name name
                             :category category
                             :note note
                             :start (chronicity:parse start)
                             :end (chronicity:parse end)
                             :location location)
              (cal:events (calendar (month-pane *application-frame*)))))
        (setf (frame-current-layout *application-frame*) 'default)
        (change-space-requirements (month-pane *application-frame*) :resize-frame t)))))

(define-frame-command (com-list-events :menu t :name t) ()
  (format *debug-io* "~A~%"
          (cal:events (calendar (month-pane *application-frame*)))))

(define-frame-command (com-refresh :menu t :name t) ())

;; (define-frame-command (com-select-date :menu t :name t)
;;     ((date 'date)))

;; (define-presentation-translator timestamp-to-timestring)
