(in-package :cl)

(defpackage :chronicle.clim
  (:use :clim :clim-lisp)
  (:local-nicknames (:at :anathema)
                    (:ats :anathema.style)
                    (:t  :local-time)
                    (:cal :chronicle)
                    (:sm :stealth-mixin)
                    (:nr :named-readtables)
                    (:i  :interact)
                    (:id :interact.defs))
  (:export #:pane #:frame #:run))
