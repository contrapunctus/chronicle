(in-package :chronicle.clim)

(defun update-pane-month (by)
  (let* ((pane  (month-pane *application-frame*))
         (month (month pane)))
    (setf (month (month-pane *application-frame*))
          (t:adjust-timestamp month (offset :month by)))))

(define-frame-command (com-next-month :menu t :name t :keystroke (#\n :control)) ()
  (update-pane-month 1))

(define-frame-command (com-previous-month :menu t :name t :keystroke (#\t :control)) ()
  (update-pane-month -1))
