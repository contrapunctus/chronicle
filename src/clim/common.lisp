(in-package :chronicle.clim)

(at:define-style today (ats:constant) ())

(defmethod at:text-style-1 ((style today) theme medium)
  (make-text-style nil :bold nil))

(at:define-style holiday () ())
(at:define-style header-weekday (ats:constant) ())
(at:define-style header-weekend (ats:comment) ())
(at:define-style header-month (ats:function) ())

(define-presentation-type name () :inherit-from 'string)
(define-presentation-type category () :inherit-from 'string)
(define-presentation-type note () :inherit-from 'string)
(define-presentation-type time () :inherit-from 't:timestamp)
(define-presentation-type location () :inherit-from 'string)

(defclass cursor-mixin ()
  ((%cursor :initarg :cursor :accessor cursor :initform nil)))

(sm:define-stealth-mixin calendar-date-mixin (cursor-mixin) t:timestamp ())

(defclass common-pane (application-pane) ()
  (:default-initargs :display-function 'display-pane
                     :incremental-redisplay t
                     :foreground (at:style-foreground-ink ats:*default*)
                     :background (at:style-background-ink ats:*default*)))

(defgeneric display-pane (frame stream))
