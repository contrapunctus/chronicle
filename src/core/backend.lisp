(in-package :chronicle)

(defclass backend ()
  ((%calendars :initarg :calendars :accessor calendars))
  (:documentation "Protocol class for `chronicle' backends."))

(defclass file-backend-mixin ()
  ((%base-name :initform "data"
               :initarg :base-name
               :reader base-name
               :documentation "Base name for backend file, without directory or extension.")
   (%extension :initform nil
               :initarg :extension
               :reader extension
               :documentation "Extension used for files of this backend."))
  (:documentation "Mixin for backends storing data in a single file."))

(defvar *xdg-data-pathname*
  (merge-pathnames #P"chronicle/" (uiop:xdg-data-pathname)))

(defmethod initialize-instance :after
    ((backend file-backend-mixin) &rest initargs &key &allow-other-keys)
  (ensure-directories-exist *xdg-data-pathname*))

(defgeneric absolute-pathname (backend)
  (:documentation "Return the absolute pathname for BACKEND's data."))

(defmethod absolute-pathname ((backend file-backend-mixin))
  (make-pathname :type (extension backend)
                 :name (base-name backend)
                 :defaults *xdg-data-pathname*))

(defvar *active-backend* nil)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun earmuff-symbol (symbol)
    (intern (concatenate 'string "*" (symbol-name symbol) "*"))))

(defmacro define-backend (name direct-superclasses direct-slots &rest options)
  "Define a new backend class called NAME, and a variable `*name*'
holding an instance of it."
  (let ((var-name (earmuff-symbol name)))
    `(progn
       (defclass ,name ,direct-superclasses ,direct-slots ,@options)
       (defvar ,var-name (make-instance (quote ,name)))
       (unless *active-backend*
         (setf *active-backend* ,var-name)))))

(defgeneric migrate (from to))
