(in-package :chronicle)

(defclass calendar-view-item-mixin () ())
(defclass task-view-item-mixin () ())

(defclass entry ()
  ((%name :accessor name :initarg :name :initform "Unnamed Event")
   (%notes :accessor notes :initarg :notes :initform nil)
   (%location :accessor location :initarg :location :initform nil)
   (%category :accessor category :initarg :category :initform nil)
   (%start-time :accessor start-time :type timestamp :initarg :start-time :initform (get-appropriate-time))
   (%end-time :accessor end-time :type (or timestamp null) :initarg :end-time :initform nil)
   (%repeat :accessor repeat :initarg :repeat :initform nil)
   (%reminders :accessor reminders :initarg :reminders :initform '())
   (%hypothetical-unsaveable-slot :accessor hypothetical-unsaveable-slot)
   (%unusable-initargs :accessor unusable-initargs :initarg :unusable :initform '())))

(defmethod print-object ((object entry) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "\"~A\"" (name object))))

#+(or)
(defmethod print-object ((object entry) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~{~A~^  ~}"
            (list (name object)
                  (t:format-timestring nil (start object)
                                       :format t:+rfc-1123-format+)
                  (t:format-timestring nil (end object)
                                       :format t:+rfc-1123-format+)))))

(defgeneric add-entry (backend entry calendar))
(defgeneric remove-entry (backend entry calendar))

(defgeneric entry-equal (a b)
  (:method-combination and :most-specific-last))

(defmethod entry-equal ((a entry) (b entry))
  (and (equal (name a) (name b))
       (equal (category a) (category b))
       (equal (note a) (note b))
       (equal (location a) (location b))
       (t:timestamp= (start a) (start b))
       (t:timestamp= (end a) (end b))
       (equal (repeat a) (repeat b))))

(defmethod duration-as-string ((entry entry))
  (with-output-to-string (stream)
    (format stream "~A" (start-time entry))
    (when (end-time entry)
      (format stream " ~~ ~A" (end-time entry)))))

(defclass event (entry calendar-view-item-mixin)
  ((%lasts-all-day-p :accessor lasts-all-day-p :initarg :lasts-all-day-p :initform nil)))

(defmethod print-object ((object event) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "\"~A\" ~A"
            (name object) (timestamp-pretty-string (start-time object)))
    (when (end-time object)
      (format stream " ~~ ~A" (timestamp-pretty-string (end-time object))))))

(defmethod in-the-past-p ((event event))
  (t:timestamp< (if (end-time event) (end-time event) (start-time event)) (t:now)))

(defmethod in-the-future-p ((event event))
  (t:timestamp< (t:now) (start-time event)))

(defmethod happening-now-p ((event event))
  (when (end-time event)
    (and (t:timestamp>= (end-time event) (t:now))
         (t:timestamp<= (start-time event) (t:now)))))

(defclass task (entry task-view-item-mixin)
  ((%completed-p :accessor completed-p :initform nil)))

(defmethod entry-equal ((a task) (b task))
  (equal (completed-p a) (completed-p b)))

(defmethod in-the-past-p ((task task))
  (t:timestamp< (deadline task) (t:now)))

(defmethod overdue-p ((task task))
  (and (not (completed-p task)) (in-the-past-p task)))
