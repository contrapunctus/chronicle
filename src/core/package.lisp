(in-package :cl)

(defpackage :chronicle
  (:use :cl)
  (:local-nicknames (#:t #:local-time)
                    (#:msgbox #:org.shirakumo.messagebox))
  (:export #:make-timestamp #:timestamp-pretty-string)

  (:export #:calendar #:name #:events
           #:add-calendar #:remove-calendar #:calendar-equal
           #:list-events #:sorted-events #:future-events)

  (:export #:calendar-view-item-mixin #:task-view-item-mixin
           #:entry #:event #:task)

  (:export #:entry #:name #:notes #:location #:category #:start-time #:end-time #:repeat
           #:reminders #:lasts-all-day-p #:completed-p)

  (:export #:add-entry #:remove-entry #:entry-equal
           #:in-the-past-p #:in-the-future-p #:happening-now-p #:overdue-p
           #:categories #:add-category)

  (:export #:backend #:calendars
           #:file-backend-mixin #:base-name #:extension #:absolute-pathname
           #:define-backend #:*active-backend*
           #:migrate))
