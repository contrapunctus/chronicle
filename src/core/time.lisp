(in-package :chronicle)

(defvar *home-timezone* t:*default-timezone*)

(defun make-timestamp (year month day &key (hour 0) (minute 0) (second 0) (nsec 0))
  (t:encode-timestamp nsec second minute hour day month year))

(defun get-appropriate-time ()
  "Automagically returns an appropriate time based on what's going on in the UI.
TODO: Make something that lets a UI actually affect this."
  (multiple-value-bind (second minute hour day month year)
      (get-decoded-time)
    (declare (ignore second))
    (make-timestamp year month day :hour hour :minute minute)))

(defun timestamp-pretty-string (timestamp &key time-only-p)
  (let* ((time-only '((:hour 2) #\: (:min 2)))
         (full (append
                '((:year 4) #\- (:month 2) #\- (:day 2) #\Space)
                time-only)))
    (t:format-timestring nil timestamp :format (if time-only-p time-only full))))
