(in-package :chronicle)

(defclass calendar ()
  ((%name :accessor name :initarg :name :initform "Unnamed Calendar")
   (%events :accessor events
            :initform '()
            :initarg :events)))

(defgeneric add-calendar (backend calendar))
(defgeneric remove-calendar (backend calendar))
(defgeneric list-events (backend calendar))
(defgeneric sorted-events (backend calendar))
(defgeneric future-events (backend calendar))

(defun calendar-equal (a b)
  (and (equal (name a) (name b))
       (loop for ea in (events a)
             for eb in (events b)
             always (entry-equal ea eb))))

(defun print-all-events (calendar)
  (loop :for event :in (sorted-events calendar
                                      (lambda (event-a event-b)
                                        (t:timestamp< (start-time event-a)
                                                      (start-time event-b))))
        :do (format t "\"~A\" ~A ~C ~A~%"
                    (name event) (start-time event) #\~ (end-time event))))
