(in-package :chronicle)

;;; Reminders (none of this is to be considered finished, I made it in a hurry)
(defclass reminder ()
  ((%event-name :accessor event-name :initarg :event-name)
   (%event-start-time :accessor event-start-time :initarg :event-start-time)))

(defgeneric should-fire-p (reminder))
(defgeneric has-fired-p (reminder))
(defgeneric fire-reminder (reminder))

(defclass standard-reminder (reminder)
  ((%reminder-text :accessor reminder-text :initarg :reminder-text)
   (%reminder-time :accessor reminder-time :initarg :reminder-time)
   (%has-fired-p :accessor has-fired-p :initform nil :initarg :has-fired-p)))

(defmethod simplify-for-saving ((obj standard-reminder))
  (make-object-saveable obj))

(defmethod fire-reminder ((reminder standard-reminder))
  (unless (has-fired-p reminder)
    (setf (has-fired-p reminder) t)
    (msgbox:show (format nil "Calendar reminder!~%~A" (reminder-text reminder)))))

(defmethod should-fire-p ((reminder standard-reminder))
  (and (not (has-fired-p reminder))
       (t:timestamp< (reminder-time reminder) (t:now))))

(defun make-reminder-for-event (event &optional reminder-time)
  (let ((reminder-time (or reminder-time (start-time event))))
    (make-instance 'standard-reminder
                   :reminder-text (format nil "~A~%~A~%Duration: ~A"
                                          (name event)
                                          (location event)
                                          (duration-as-string event))
                   :reminder-time reminder-time
                   :event-name (name event)
                   :event-start-time (start-time event))))

(defmethod add-reminder ((reminder reminder) (entry entry))
  (push reminder (reminders entry))
  reminder)

;;; Reminder monitor (also made in a hurry because I needed reminders)
(defvar *reminders* '())
(defparameter *quit-reminder-monitor-p* nil)
(defparameter *reminder-monitor-delay* 5
  "How many seconds to wait between each reminder check.")

(defun reminder-monitor (&key calendar-name refresh-each-check-p)
  (let (calendar)
    (flet ((load-cal ()
             (setf calendar (load-calendar-from-file calendar-name))))
      (when calendar-name
        (load-cal)
        (register-all-reminders calendar))
      (do ((reminders (copy-list *reminders*)
                      (if refresh-each-check-p
                          (progn
                            (load-cal)
                            (register-all-reminders calendar)
                            *reminders*)
                          (copy-list *reminders*))))
          (*quit-reminder-monitor-p*)
        (dolist (reminder *reminders*)
          (when (should-fire-p reminder)
            (fire-reminder reminder)
            (save-calendar-to-file calendar)))
        (sleep *reminder-monitor-delay*))))
  (format t "Quit the reminder monitor.~%")
  (force-output))

(defun register-reminder (reminder)
  (push reminder *reminders*))

(defun register-all-reminders (calendar)
  (dolist (reminders (mapcar 'reminders (list-events calendar)))
    (dolist (reminder (remove-if #'has-fired-p reminders))
      (register-reminder reminder)))
  (setf *reminders* (remove-duplicates *reminders*)))
