(defsystem     "chronicle-sexp"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "John Lorentzson (Duuqnd)"
  :description "s-expression backend for Chronicle"
  :depends-on  (#:chronicle #:closer-mop #:str #:messagebox)
  :serial      t
  :components  ((:module "src/backends/"
                 :components ((:file "sexp")))))
