(defsystem     "chronicle-tui"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "John Lorentzson (Duuqnd)"
  :description "TUI for Chronicle"
  :depends-on  (#:chronicle #:local-time #:closer-mop #:str #:messagebox)
  :serial      t
  :components  ((:module "src/"
                 :components ((:file "crappy-tui")))))
