(defsystem     "chronicle-clim"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :maintainer  "contrapunctus <contrapunctus at disroot dot org>"
  :description "CLIM frontend for Chronicle"
  :depends-on  (#:mcclim #:chronicle #:stealth-mixin #:named-readtables #:fn #:interact)
  :serial t
  :components  ((:module "src/clim/"
                 :serial t
                 :components ((:file "package")
                              (:file "common")
                              (:file "pane-month")
                              (:file "pane-input")
                              (:file "frame")
                              (:module "commands"
                               :serial t
                               :components ((:file "month")
                                            (:file "cursor")))
                              (:file "interact")))))
