(defsystem     "chronicle-clobber"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :maintainer  "contrapunctus <contrapunctus at disroot dot org>"
  :description "Clobber backend for Chronicle"
  :depends-on  (:chronicle :clobber)
  :serial      t
  :components  ((:module "src/backends/"
                 :components ((:file "clobber")))))
