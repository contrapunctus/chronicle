# Created 2022-12-03 Sat 06:00
#+options: prop:t toc:2
#+title: chronicle
#+author: contrapunctus
#+export_file_name: chronicle.org

* Description
:PROPERTIES:
:CUSTOM_ID: description
:END:
Calendar made using CLIM. Uses [[https://codeberg.org/contrapunctus/anathema][Anathema]] for theming.

[[file:2022-08-17 14-04-49.png]]

It is meant to serve at least two purposes -

1. A calendar application, displaying the user's engagements, and helping them schedule future engagements.

2. A date picker widget for [[http://metamodular.com/closos.pdf][CLOSOS]], permitting the selection of dates within the context of the user's engagements.

** Thanks
:PROPERTIES:
:CREATED:  2022-09-13T14:25:06+0530
:CUSTOM_ID: description-thanks
:END:
Robert Strandh (beach) provided funding, code review, and design input.

John Lorentzson (Duuqnd) merged his =clos-calendar= project into this one, providing the =sexp= backend and much of the core library.

Daniel Kochmanski (jackdaniel), scymtym, shka/sirherrbatka and many others in the Lisp community helped me out a great deal by answering my CLIM and Common Lisp questions.

* Reference
:PROPERTIES:
:CREATED:  2022-09-17T22:20:08+0530
:CUSTOM_ID: reference
:END:
** Calendars
:PROPERTIES:
:CREATED:  2022-09-17T22:25:30+0530
:CUSTOM_ID: reference-calendar
:END:
#+results:
**** =calendar (standard-object)=                                     :class:
:PROPERTIES:
:CUSTOM_ID: reference-calendar
:END:

*** Accessors
:PROPERTIES:
:CREATED:  2022-09-18T12:36:35+0530
:CUSTOM_ID: reference-calendar-accessors
:END:
#+results:
**** =name (object)=                                       :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-name
:END:
**** =events (object)=                                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-events
:END:

*** Operations
:PROPERTIES:
:CREATED:  2022-09-18T12:36:39+0530
:CUSTOM_ID: reference-calendar-operations
:END:
#+results:
**** =add-calendar (backend calendar)=                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-add-calendar
:END:
**** =remove-calendar (backend calendar)=                  :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-remove-calendar
:END:
**** =calendar-equal (a b)=                                        :function:
:PROPERTIES:
:CUSTOM_ID: reference-calendar-equal
:END:
**** =list-events (backend calendar)=                      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-list-events
:END:
**** =sorted-events (backend calendar)=                    :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-sorted-events
:END:
**** =future-events (backend calendar)=                    :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-future-events
:END:

** Entries
:PROPERTIES:
:CREATED:  2022-09-17T22:25:43+0530
:CUSTOM_ID: reference-entry
:END:
#+results:
**** =calendar-view-item-mixin (standard-object)=                     :class:
:PROPERTIES:
:CUSTOM_ID: reference-calendar-view-item-mixin
:END:
**** =task-view-item-mixin (standard-object)=                         :class:
:PROPERTIES:
:CUSTOM_ID: reference-task-view-item-mixin
:END:
**** =entry (standard-object)=                                        :class:
:PROPERTIES:
:CUSTOM_ID: reference-entry
:END:
**** =event (chronicle:entry chronicle:calendar-view-item-mixin)=     :class:
:PROPERTIES:
:CUSTOM_ID: reference-event
:END:
**** =task (chronicle:entry chronicle:task-view-item-mixin)=          :class:
:PROPERTIES:
:CUSTOM_ID: reference-task
:END:

*** Accessors
:PROPERTIES:
:CREATED:  2022-09-18T12:39:45+0530
:CUSTOM_ID: reference-entry-accessors
:END:
#+results:
**** =name (object)=                                       :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-name
:END:
**** =notes (object)=                                      :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-notes
:END:
**** =location (object)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-location
:END:
**** =category (object)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-category
:END:
**** =start-time (object)=                                 :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-start-time
:END:
**** =end-time (object)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-end-time
:END:
**** =repeat (object)=                                     :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-repeat
:END:
**** =reminders (object)=                                  :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-reminders
:END:
**** =lasts-all-day-p (object)=                            :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-lasts-all-day-p
:END:
**** =completed-p (object)=                                :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-completed-p
:END:

*** Operations
:PROPERTIES:
:CREATED:  2022-09-18T12:40:28+0530
:CUSTOM_ID: reference-entry-operations
:END:
#+results:
**** =add-entry (backend entry calendar)=                  :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-add-entry
:END:
**** =remove-entry (backend entry calendar)=               :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-remove-entry
:END:
**** =entry-equal (a b)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-entry-equal
:END:
**** =in-the-past-p (event)=                               :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-in-the-past-p
:END:
**** =in-the-future-p (event)=                             :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-in-the-future-p
:END:
**** =happening-now-p (event)=                             :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-happening-now-p
:END:
**** =overdue-p (task)=                                    :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-overdue-p
:END:

** Backends
:PROPERTIES:
:CREATED:  2022-09-17T22:26:13+0530
:CUSTOM_ID: reference-backend
:END:
#+results:
*** =backend (standard-object)=                                       :class:
:PROPERTIES:
:CUSTOM_ID: reference-backend
:END:
Protocol class for =chronicle= backends.

*** =calendars (object)=                                   :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-calendars
:END:
*** =file-backend-mixin (standard-object)=                            :class:
:PROPERTIES:
:CUSTOM_ID: reference-file-backend-mixin
:END:
Mixin for backends storing data in a single file.

*** =absolute-pathname (backend)=                          :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-absolute-pathname
:END:
Return the absolute pathname for BACKEND's data.

*** =*active-backend*=                                             :variable:
:PROPERTIES:
:CUSTOM_ID: reference-*active-backend*
:END:
*** =define-backend (name direct-superclasses direct-slots &rest options)= :macro:function:
:PROPERTIES:
:CUSTOM_ID: reference-define-backend
:END:
Define a new backend class called NAME, and a variable =*name*= holding an instance of it.

*** =migrate (from to)=                                    :generic:function:
:PROPERTIES:
:CUSTOM_ID: reference-migrate
:END:

** Time
:PROPERTIES:
:CREATED:  2022-09-18T11:38:19+0530
:CUSTOM_ID: reference-time
:END:
#+results:
*** =make-timestamp (year month day &key (hour 0) (minute 0) (second 0) (nsec 0))= :function:
:PROPERTIES:
:CUSTOM_ID: reference-make-timestamp
:END:
*** =timestamp-pretty-string (timestamp &key time-only-p)=         :function:
:PROPERTIES:
:CUSTOM_ID: reference-timestamp-pretty-string
:END:
